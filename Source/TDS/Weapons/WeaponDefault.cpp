// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ProjectileSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ProjectileSpawnPoint->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

// Called every frame
void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{
		if (bWeaponFiring)
			if (FireTimer < 0.f)
			{
				if (!bWeaponReloading)
					Fire();
			}
			else
				FireTimer -= DeltaTime;
	}
	else
	{
		if (!bWeaponReloading)
		{
			InitReload();
		}
	}
}

// Called every frame
void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

// Called every frame
void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bWeaponReloading)
	{
		if (!bWeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}				

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if(ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	UpdateStateWeapon(EMovementState::DefaultState);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		bWeaponFiring = bIsFire;
	}
	else
	{
		bWeaponFiring = false;
		FireTimer = 0.01f; //!!!!
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileSettings AWeaponDefault::GetProjectile()
{	
	return WeaponSettings.ProjectileSettings;
}

FProjectileSettings AWeaponDefault::GetProjectileSettings()
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSettings.RateOfFire;
	WeaponStats.Round--;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(
		GetWorld(),
		WeaponSettings.SoundFireWeapon,
		ProjectileSpawnPoint->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(
		GetWorld(),
		WeaponSettings.EffectFireWeapon,
		ProjectileSpawnPoint->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ProjectileSpawnPoint)
	{
		FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
		FRotator SpawnRotation = ProjectileSpawnPoint->GetComponentRotation();
		FProjectileSettings ProjectileSettings;
		ProjectileSettings = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
			{
			EndLocation = GetFireEndLocation(); 

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(
				Dir,
				FVector(0, 1, 0),
				FVector(0, 0, 1),
				FVector::ZeroVector);
			
			SpawnRotation = myMatrix.Rotator();

			if (ProjectileSettings.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(
					GetWorld()->SpawnActor(
						ProjectileSettings.Projectile,
						&SpawnLocation,
						&SpawnRotation,
						SpawnParams));
				
				if (myProjectile)
				{													
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);				
				}
			}
			else
			{
				//ToDo Projectile null Init trace fire			
				
				//GetWorld()->LineTraceSingleByChannel()
			}
			}				
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::DefaultState:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.DefaultStateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.DefaultStateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.DefaultStateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.DefaultStateDispersionReduction;
		break;
		
	case EMovementState::SprintState:
		BlockFire = true;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
		
	case EMovementState::AimState:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimStateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimStateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.AimStateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimStateDispersionReduction;
		break;
		
	case EMovementState::AimWalkState:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimWalkStateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimWalkStateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.AimWalkStateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimWalkStateDispersionReduction;
		break;
		
	case EMovementState::WalkState:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.WalkStateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.WalkStateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.WalkStateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.WalkStateDispersionReduction;
		break;
		
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{		
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	
	FVector tempVec = (ProjectileSpawnPoint->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if(tempVec.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ProjectileSpawnPoint->GetComponentLocation();
		EndLocation += ApplyDispersionToShoot(
			(ProjectileSpawnPoint->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if(ShowDebug)
			DrawDebugCone(
				GetWorld(),
				ProjectileSpawnPoint->GetComponentLocation(),
				-(ProjectileSpawnPoint->GetComponentLocation() - ShootEndLocation),
				WeaponSettings.TraceDistance,
				GetCurrentDispersion()* PI / 180.f,
				GetCurrentDispersion()* PI / 180.f,
				32,
				FColor::Emerald,
				false,
				.1f, (uint8)'\000',
				1.0f);
	}	
	else
	{
		EndLocation = ProjectileSpawnPoint->GetComponentLocation();
		EndLocation += ApplyDispersionToShoot(ProjectileSpawnPoint->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(
				GetWorld(),
				ProjectileSpawnPoint->GetComponentLocation(),
				ProjectileSpawnPoint->GetForwardVector(),
				WeaponSettings.TraceDistance,
				GetCurrentDispersion()* PI / 180.f,
				GetCurrentDispersion()* PI / 180.f,
				32,
				FColor::Emerald,
				false,
				.1f,
				(uint8)'\000',
				1.0f);
	}
		

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(
			GetWorld(),
			ProjectileSpawnPoint->GetComponentLocation(),
			ProjectileSpawnPoint->GetComponentLocation() + ProjectileSpawnPoint->GetForwardVector() * 500.0f,
			FColor::Cyan,
			false,
			5.f,
			(uint8)'\000',
			0.5f);
		//direction projectile must fly
		DrawDebugLine(
			GetWorld(),
			ProjectileSpawnPoint->GetComponentLocation(),
			ShootEndLocation,
			FColor::Red,
			false,
			5.f,
			(uint8)'\000',
			0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(
			GetWorld(),
			ProjectileSpawnPoint->GetComponentLocation(),
			EndLocation,
			FColor::Black,
			false,
			5.f,
			(uint8)'\000',
			0.5f);

		//DrawDebugSphere(
		//GetWorld(),
		//ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic,
		//10.f,
		//8,
		//FColor::Red,
		//false,
		//4.0f);
	}
	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponStats.Round;
}

void AWeaponDefault::InitReload()
{
	bWeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;

	//ToDo Anim reload
	if(WeaponSettings.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimCharReload);
}

void AWeaponDefault::FinishReload()
{
	bWeaponReloading = false;
	WeaponStats.Round = WeaponSettings.MaxRound;

	OnWeaponReloadEnd.Broadcast();
}


