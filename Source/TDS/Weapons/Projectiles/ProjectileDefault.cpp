// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollisionSphere"));

	ProjectileCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	ProjectileCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	ProjectileCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
	
	ProjectileCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	ProjectileCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	RootComponent = ProjectileCollisionSphere;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->SetCanEverAffectNavigation(false);

	ProjectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	ProjectileFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	ProjectileMovement->UpdatedComponent = RootComponent;
	ProjectileMovement->InitialSpeed = 1.f;
	ProjectileMovement->MaxSpeed = 0.f;

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefault::InitProjectile(FProjectileSettings InitParam)
{
	ProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	ProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	ProjectileSettings = InitParam;
}

void AProjectileDefault::BulletCollisionSphereHit(
	UPrimitiveComponent* HitComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSettings.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileSettings.HitDecals[mySurfacetype];

			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(
					myMaterial,
					FVector(20.0f),
					OtherComp,
					NAME_None,
					Hit.ImpactPoint,
					Hit.ImpactNormal.Rotation(),
					EAttachLocation::KeepWorldPosition,
					10.0f);
			}
		}
		if (ProjectileSettings.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSettings.HitFXs[mySurfacetype];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(
					Hit.ImpactNormal.Rotation(),
					Hit.ImpactPoint,
					FVector(1.0f)));
			}
		}
			
		if (ProjectileSettings.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.HitSound, Hit.ImpactPoint);
		}
	
	}
	UGameplayStatics::ApplyDamage(
		OtherActor,
		ProjectileSettings.ProjectileDamage,
		GetInstigatorController(),
		this,
		NULL);
	ImpactProjectile();	
	//UGameplayStatics::ApplyRadialDamageWithFalloff()
	//Apply damage cast to if char like bp? //OnAnyTakeDmage delegate
	/*UGameplayStatics::ApplyDamage(
		OtherActor,
		ProjectileSettings.ProjectileDamage,
		GetOwner()->GetInstigatorController(),
		GetOwner(),
		NULL);*/
	//or custom damage by health component
	
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}
