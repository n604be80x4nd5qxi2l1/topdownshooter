// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDSPlayerController.generated.h"

UCLASS()
class ATDSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDSPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	//uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	/*virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. #1#
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. #1#
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. #1#
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. #1#
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. #1#
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();*/
};


