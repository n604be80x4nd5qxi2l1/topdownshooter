// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponSettingsByName(FName WeaponName, FWeaponSettings& WeaponSettings)
{
	bool bIsFind = false;
	

	if (WeaponSettingsTable)
	{
		FWeaponSettings* WeaponSettingsRow = WeaponSettingsTable->FindRow<FWeaponSettings>(WeaponName, "", false);
		if (WeaponSettingsRow)
		{
			bIsFind = true;
			WeaponSettings = *WeaponSettingsRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}
		
	return bIsFind;
}
