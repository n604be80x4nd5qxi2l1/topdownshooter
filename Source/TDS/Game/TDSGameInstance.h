// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDS/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TDS/Weapons/WeaponDefault.h"

#include "TDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings ")
	UDataTable* WeaponSettingsTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponSettingsByName(FName WeaponName, FWeaponSettings& WeaponSettings);
	
};