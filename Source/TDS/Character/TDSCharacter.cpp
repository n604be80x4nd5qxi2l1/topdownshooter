// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "TDS/Game/TDSPlayerController.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS/Game/TDSGameInstance.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	//GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	//GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	/*CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());*/

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(
			GetWorld(),
			CursorMaterial,
			CursorSize,
			FVector(0));
	}
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	
	if (CurrentCursor)
	{
		APlayerController* PlayerController = Cast<APlayerController>(GetController());
		if (PlayerController)
		{
			FHitResult TraceHitResult;
			PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorForwardVector = TraceHitResult.ImpactNormal;
			FRotator CursorRotation = CursorForwardVector.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorRotation);
		}
	}
	
	MoveCharacterTick(DeltaSeconds);
	StaminaTick(DeltaSeconds);
	
	//Show stamina value
	auto Msg = FString::SanitizeFloat(Stamina);
	GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds, FColor::Yellow, Msg);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"),this, &ATDSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"),this, &ATDSCharacter::InputAxisY);

	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::WeaponTryReload);
}

void ATDSCharacter::InputAxisX(float AxisValue)
{
	AxisX = AxisValue;
}

void ATDSCharacter::InputAxisY(float AxisValue)
{
	AxisY = AxisValue;
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	if (MovementState == EMovementState::DefaultState)
	{
		FVector myRotationVector = FVector(AxisX,AxisY,0.0f);
		FRotator myRotator = myRotationVector.ToOrientationRotator();
		SetActorRotation((FQuat(myRotator)));
	}
	else
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			float RotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, RotatorResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::DefaultState:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintState:
					break;
				case EMovementState::AimState:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalkState:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					break;
				case EMovementState::WalkState:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				default:
					break;
				}
				
				
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				//aim cursor like 3d Widget?
			}
		}
	}		
}

void ATDSCharacter::MoveCharacterTick(float DeltaSeconds)
{
	const APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	FVector CursorLocation, CursorDirection, ActorLocation;
	ActorLocation = GetActorLocation();
	PlayerController->DeprojectMousePositionToWorld(CursorLocation, CursorDirection);
	
	//-1*((CursorLocationZ-ActorLocationZ)/CursorDirectionZ)*CursorDirectionXYZ)+CursorLocationXYZ
	const auto CursorPosition(-1*((CursorLocation.Z-ActorLocation.Z)/CursorDirection.Z*CursorDirection)+CursorLocation);
	
	const auto CharacterToCursorDirection(UKismetMathLibrary::GetDirectionUnitVector(
			GetActorLocation(),
			CursorPosition));
	ToCursorDirection = CharacterToCursorDirection;

	FRotator NewRotation(GetActorRotation().Pitch,
	UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorPosition).Yaw,
	GetActorRotation().Roll);

	const auto TickRotation = UKismetMathLibrary::RInterpTo(
		GetActorRotation(),
		NewRotation,
		GetWorld()->DeltaTimeSeconds,
		RotateSpeed);
		
	ChangeMovementState(MovementState);
	
	if (MovementState == EMovementState::SprintState)
	{
		const float XSign = AxisX * CharacterToCursorDirection.X;
		const float YSign = AxisY * CharacterToCursorDirection.Y;
		
		if ((XSign < 0 || YSign < 0 || Stamina < DeltaSeconds) || (AxisX == 0.f && AxisY == 0.f))
		{
			GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed.DefaultSpeed;
		}
	}

	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);
	SetActorRotation(TickRotation);
	
	if (CurrentWeapon)
	{
		FVector Displacement = FVector(0);
		switch (MovementState)
		{
		case EMovementState::DefaultState:
			Displacement = FVector(0.0f, 0.0f, 120.0f);
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		case EMovementState::SprintState:
			break;
		case EMovementState::AimState:
			Displacement = FVector(0.0f, 0.0f, 160.0f);
			CurrentWeapon->ShouldReduceDispersion = true;
			break;
		case EMovementState::AimWalkState:
			CurrentWeapon->ShouldReduceDispersion = true;
			Displacement = FVector(0.0f, 0.0f, 160.0f);
			break;
		case EMovementState::WalkState:
			Displacement = FVector(0.0f, 0.0f, 120.0f);
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		default:
			break;
		}

		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
		{
			CurrentWeapon->ShouldReduceDispersion = true;
		}
				
		CurrentWeapon->ShootEndLocation = CursorPosition;//+ Displacement;
		//aim cursor like 3d Widget?
	}
}

void ATDSCharacter::StaminaTick(float DeltaSeconds)
{
	if (GetCharacterMovement()->MaxWalkSpeed <= CharacterSpeed.DefaultSpeed)
	{
		if (Stamina < MaxStamina)
			Stamina+=DeltaSeconds*StaminaIncreaseSpeed;
		else
			Stamina = MaxStamina;
	}
	else
	{
		if (Stamina < DeltaSeconds)
			Stamina = 0.f;
		else
			Stamina-=DeltaSeconds*StaminaDecreaseSpeed;	
			
	}
}


void ATDSCharacter::CharacterUpdate()
{
	float ResultSpeed = CharacterSpeed.DefaultSpeed;
	
	switch (MovementState)
	{
	case EMovementState::DefaultState:
		ResultSpeed = CharacterSpeed.DefaultSpeed;
		break;
	case EMovementState::SprintState:
		ResultSpeed = CharacterSpeed.SprintSpeed;
		break;
	case EMovementState::WalkState:
		ResultSpeed = CharacterSpeed.WalkSpeed;
		break;
	case EMovementState::AimState:
		ResultSpeed = CharacterSpeed.DefaultSpeed - CharacterSpeed.AimSpeedPenalty;
		break;
	case EMovementState::AimWalkState:
		ResultSpeed = CharacterSpeed.WalkSpeed - CharacterSpeed.AimSpeedPenalty;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	LastMovementState = MovementState;
	MovementState = NewMovementState;
	CharacterUpdate();

	AWeaponDefault* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::WeaponInit(FName WeaponName)
{
	UTDSGameInstance* GameInstance = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponSettings WeaponSettings;
	if (GameInstance)
	{
		if (GameInstance->GetWeaponSettingsByName(WeaponName, WeaponSettings))
		{
			if (WeaponSettings.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* NewWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(WeaponSettings.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (NewWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					NewWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = NewWeapon;
					
					NewWeapon->WeaponSettings = WeaponSettings;
					NewWeapon->WeaponStats.Round = WeaponSettings.MaxRound;
					
					//Remove !!! Debug
					NewWeapon->ReloadTime = WeaponSettings.ReloadTime;
					
					NewWeapon->UpdateStateWeapon(MovementState);

					NewWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					NewWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDSCharacter::WeaponTryReload()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSettings.MaxRound)
			CurrentWeapon->InitReload();
	}
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDSCharacter::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation()
{
	//in bp
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in bp
}

